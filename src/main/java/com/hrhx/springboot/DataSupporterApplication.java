package com.hrhx.springboot;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * DataSupporterApplication
 * @author duhongming
 *
 */
//@EnableDiscoveryClient
@SpringBootApplication
//强制使用cglib代理
//@EnableAspectJAutoProxy(proxyTargetClass = true)
//EhCache（第一步）：注解开启缓存功能
//@EnableCaching
public class DataSupporterApplication{
	public static void main(String[] args) {  
        SpringApplication.run(DataSupporterApplication.class,args);
    }  
}
