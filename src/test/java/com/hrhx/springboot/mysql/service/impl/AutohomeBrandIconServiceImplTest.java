package com.hrhx.springboot.mysql.service.impl;

import com.hrhx.springboot.DataSupporterApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
@SpringBootTest(classes = DataSupporterApplication.class)
@RunWith(SpringRunner.class)
public class AutohomeBrandIconServiceImplTest {
    @Autowired
    private AutohomeBrandIconServiceImpl service;
    @Test
    public void test() throws Exception {
        service.updateIcon();
    }
}